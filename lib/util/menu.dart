import 'routes.dart';

/// Contains all possible popupmenus' strings
class Menu {
  static const home = {
    'app.menu.about': Routes.about,
    'app.menu.settings': Routes.settings,
  };

  static const launch = [
    'fitness.launch.menu.reddit',
    'fitness.launch.menu.press_kit',
    'fitness.launch.menu.article',
  ];

  static const wikipedia = [
    'fitness.other.menu.wikipedia',
  ];

  static const ship = [
    'fitness.other.menu.marine_traffic',
  ];

  static const fitnessApp = [
    'fitness.other.menu.fitnessApp',
  ];
}
