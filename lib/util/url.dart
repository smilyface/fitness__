/// Has all urls used in the app as static const strings.
class Url {
  //Base URLs
  static const fitnessAppBaseUrl = 'https://trkdig.com/';

  static const authUrl = '${fitnessAppBaseUrl}auth/signin';
  static const workoutUrls = '${fitnessAppBaseUrl}content/workout/';
  static const contentUrls = '${fitnessAppBaseUrl}content/';
  static const userUrl = 'user.$fitnessAppBaseUrl';
  static const mealUrl = 'meal.$fitnessAppBaseUrl';

  // Details URLs
  static const core = '/cores/';
  static const capsule = '/capsules/';
  static const launchpad = '/launchpads/';
  static const landpad = '/landpads/';

  // SpaceX info URLs
  static const companyInformation = '/info';
  static const companychievements = '/history';

  // Share details message
  static const shareDetails = '#fitnessApp';

  static const changelog = 'https://gitlab.com/company/project/changelog';
  static const companyUrl = 'https://company.com';

  static const companyEmail = 'app@company.com';
}
