import 'package:dio/dio.dart';

import '../util/url.dart';
import './interceptors.dart';

/// Serves data to several data repositories.
///
/// Makes http calls to several services, including
/// the open source r/SpaceX REST API.

class ApiService {
  static final ApiService _singleton = ApiService._internal();

  Dio dio;

  factory ApiService() {
    return _singleton;
  }

  ApiService._internal() {
    dio = Dio();
    dio.interceptors.add(CustomInterceptors());
  }

  Map queryParameters = {
    'page': 1,
    'limit': 10,
    'notEmpty': true,
  };

  /// Retrieves information about a specific meal.
  Future<Response> getMeal(String id) async {
    return dio.get(Url.workoutUrls + id);
  }

  /// Retireves information about all meals
  Future<Response> getMeals() async {
    return dio.get(Url.contentUrls, queryParameters: {...queryParameters});
  }

  /// Retrieves a list of all workouts
  Future<Response> getWorkouts() async {
    return dio.get(Url.workoutUrls, queryParameters: {...queryParameters});
  }

  /// Retrieves information about a specific workout.
  Future<Response> getWorkout(String id) async {
    return dio.get(Url.workoutUrls + id);
  }
}
