import 'package:dio/dio.dart';

import '../util/url.dart';

class CustomInterceptors extends InterceptorsWrapper {
  String token = '';
  Map auth = {'email': 'eizzzen@gmail.com', 'password': 'ContentProviderAdmin'};

  @override
  Future onRequest(RequestOptions options) async {
    if (token.isEmpty) {
      print('no token');
      var response = await Dio().post(Url.authUrl, data: auth);
      token = response.data['token'];
    } else {
      print('token exist');
    }
    options.headers['Authorization'] = 'Bearer $token';
    print("REQUEST[${options?.method}] => PATH: ${options?.path}");
    return super.onRequest(options);
  }

  @override
  Future onResponse(Response response) {
    print(
        "RESPONSE[${response?.statusCode}] => PATH: ${response?.request?.path}");

    return super.onResponse(response);
  }

  @override
  Future onError(DioError err) {
    print("ERROR[${err?.response?.statusCode}] => PATH: ${err?.request?.path}");
    // print(err.response);
    return super.onError(err);
  }
}
