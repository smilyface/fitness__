import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:intl/intl.dart';

/// Details about a specificworkout.

class MealInfo {
  final String id, name, type, /* description */ url;
  // final bool active;
  final List<String> photos;

  const MealInfo({
    this.id,
    this.name,
    this.type,
    // this.description,
    this.url,
    // this.active,
    this.photos,
  });

  String subtitle(BuildContext context) => '';

  String getPhoto(int index) => photos[index];

  String get getProfilePhoto => getPhoto(0);

  int get getPhotosCount => photos.length;

  String get getRandomPhoto => photos[Random().nextInt(getPhotosCount)];

  factory MealInfo.fromJson(Map<String, dynamic> json) {
    return MealInfo(
      id: json['id'],
      name: json['name'],
      url: json['url'],
      type: 'meal',
      photos: List<Map>.from(json['files'])
          .where((i) => i['type'] == 'preview')
          .map((i) => i['url'].toString())
          .toList()
          .cast<String>(),
    );
  }
}
