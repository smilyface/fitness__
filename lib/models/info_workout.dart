import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:intl/intl.dart';

/// Details about a specificworkout.

class WorkoutInfo {
  final String id, name, type, /* description, */ url;
  // final bool active;
  final List<String> photos;

  const WorkoutInfo({
    this.id,
    this.name,
    this.type,
    // this.description,
    this.url,
    // this.active,
    this.photos,
  });

  String subtitle(BuildContext context) => '';

  String getPhoto(int index) => photos[index];

  String get getProfilePhoto => getPhoto(0);

  int get getPhotosCount => photos.length;

  String get getRandomPhoto => photos[Random().nextInt(getPhotosCount)];

  factory WorkoutInfo.fromJson(Map<String, dynamic> json) {
    return WorkoutInfo(
      id: json['id'],
      name: json['name'],
      url: json['url'],
      type: 'workout',
      photos: List<Map>.from(json['files'])
          .where(
              (i) => i['type'] == 'image-size-2' || i['type'] == 'image-size-1')
          .map((i) => i['url'].toString())
          .toList()
          .cast<String>(),
    );
  }
}
