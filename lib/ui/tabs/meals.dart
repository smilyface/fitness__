import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:provider/provider.dart';
import 'package:row_collection/row_collection.dart';

import '../../models/index.dart';
import '../../repositories/index.dart';
import '../../util/menu.dart';
import '../../util/routes.dart';
import '../widgets/index.dart';

class MealsTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<MealsRepository>(
      builder: (context, model, child) => Scaffold(
        body: SliverPage<MealsRepository>.slide(
          title: FlutterI18n.translate(context, 'fitness.home.title'),
          slides: model.photos,
          popupMenu: Menu.home,
          body: <Widget>[
            SliverList(
              delegate: SliverChildBuilderDelegate(
                _buildMeals,
                childCount: model.meals?.length,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildMeals(BuildContext context, int index) {
    return Consumer<MealsRepository>(
      builder: (context, model, child) {
        final MealInfo meal = model.meals[index];
        return Column(children: <Widget>[
          ListCell(
            leading: ClipRRect(
              borderRadius: const BorderRadius.all(Radius.circular(8)),
              child: HeroImage.list(
                url: meal.getProfilePhoto,
                tag: meal.id,
              ),
            ),
            title: meal.name,
            subtitle: meal.subtitle(context),
            trailing: Icon(Icons.chevron_right),
            onTap: () => Navigator.pushNamed(
              context,
              Routes.meal,
              arguments: {'type': meal.type, 'id': meal.id},
            ),
          ),
          Separator.divider(indent: 72)
        ]);
      },
    );
  }
}
