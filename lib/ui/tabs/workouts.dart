import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:provider/provider.dart';
import 'package:row_collection/row_collection.dart';

import '../../models/index.dart';
import '../../repositories/index.dart';
import '../../util/menu.dart';
import '../../util/routes.dart';
import '../widgets/index.dart';

class WorkoutsTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<WorkoutsRepository>(
      builder: (context, model, child) {
        return Scaffold(
          body: SliverPage<WorkoutsRepository>.slide(
            title: FlutterI18n.translate(context, 'fitness.workout.title'),
            // slides: model.photos,
            popupMenu: Menu.home,
            body: <Widget>[
              SliverList(
                delegate: SliverChildBuilderDelegate(
                  _buildWorkout,
                  childCount: model.workouts?.length,
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  Widget _buildWorkout(BuildContext context, int index) {
    return Consumer<WorkoutsRepository>(
      builder: (context, model, child) {
        final WorkoutInfo workout = model.workouts[index];
        return Column(children: <Widget>[
          ListCell(
            leading: ClipRRect(
              borderRadius: const BorderRadius.all(Radius.circular(8)),
              child: HeroImage.list(
                url: workout.getProfilePhoto,
                tag: workout.id,
              ),
            ),
            title: workout.name,
            subtitle: workout.subtitle(context),
            trailing: Icon(Icons.chevron_right),
            onTap: () => Navigator.pushNamed(
              context,
              Routes.workout,
              arguments: {'type': workout.type, 'id': workout.id},
            ),
          ),
          Separator.divider(indent: 72)
        ]);
      },
    );
  }
}
