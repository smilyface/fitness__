import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_web_browser/flutter_web_browser.dart';
import 'package:provider/provider.dart';
import 'package:row_collection/row_collection.dart';
import 'package:share/share.dart';

import '../../models/index.dart';
import '../../repositories/workouts.dart';
import '../../util/menu.dart';
import '../../util/url.dart';
import '../widgets/index.dart';

/// This view all information about a Workout capsule model. It displays CapsuleInfo's specs.
class WorkoutPage extends StatelessWidget {
  final String id;

  const WorkoutPage(this.id);

  @override
  Widget build(BuildContext context) {
    final WorkoutInfo _workout =
        context.read<WorkoutsRepository>().getWorkout(id);
    return Scaffold(
      body: CustomScrollView(slivers: <Widget>[
        SliverBar(
          title: _workout.name,
          header: CacheImage(_workout.getProfilePhoto),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.share),
              onPressed: () => Share.share(
                FlutterI18n.translate(
                  context,
                  'fitness.other.share.capsule.body',
                  translationParams: {
                    'name': _workout.name,
                    'details': Url.shareDetails
                  },
                ),
              ),
              tooltip: FlutterI18n.translate(
                context,
                'fitness.other.menu.share',
              ),
            ),
            PopupMenuButton<String>(
              itemBuilder: (context) => [
                for (final item in Menu.wikipedia)
                  PopupMenuItem(
                    value: item,
                    child: Text(FlutterI18n.translate(context, item)),
                  )
              ],
              onSelected: (text) => FlutterWebBrowser.openWebPage(
                // url: _workout.url,
                androidToolbarColor: Theme.of(context).primaryColor,
              ),
            ),
          ],
        ),
        SliverSafeArea(
          top: false,
          sliver: SliverToBoxAdapter(
            child: RowLayout.cards(children: <Widget>[
              _descriptionCard(context),
              // _specsCard(context),
              // _thrustersCard(context),
            ]),
          ),
        ),
      ]),
    );
  }

  Widget _descriptionCard(BuildContext context) {
    final WorkoutInfo _workout =
        context.read<WorkoutsRepository>().getWorkout(id);
    return CardPage.body(
      title: FlutterI18n.translate(
        context,
        'description of a ${_workout.name}',
      ),
      body: RowLayout(children: <Widget>[
        RowIcon(
          FlutterI18n.translate(
            context,
            'some text and blabla',
          ),
          true,
        ),
        Separator.divider(),
        TextExpand('more text expand')
      ]),
    );
  }

  // Widget _specsCard(BuildContext context) {
  //   final WorkoutInfo _workout =
  //       context.read<WorkoutsRepository>().getWorkout(id);
  //   return CardPage.body(
  //     title: FlutterI18n.translate(
  //       context,
  //       'fitness.workout.capsule.specifications.title',
  //     ),
  //     body: RowLayout(children: <Widget>[]),
  //   );
  // }

  // Widget _thrustersCard(BuildContext context) {
  //   final WorkoutInfo _workout =
  //       context.read<WorkoutsRepository>().getWorkout(id);
  //   return CardPage.body(
  //     title: FlutterI18n.translate(
  //       context,
  //       'fitness.workout.capsule.thruster.title',
  //     ),
  //     body: RowLayout(children: <Widget>[]),
  //   );
  // }
}
