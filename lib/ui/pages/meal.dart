import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_web_browser/flutter_web_browser.dart';
import 'package:provider/provider.dart';
import 'package:row_collection/row_collection.dart';
import 'package:share/share.dart';

import '../../models/index.dart';
import '../../repositories/meals.dart';
import '../../util/menu.dart';
import '../../util/url.dart';
import '../widgets/index.dart';

/// This view all information about a Meal model
class MealPage extends StatelessWidget {
  final String id;

  const MealPage(this.id);

  @override
  Widget build(BuildContext context) {
    final MealInfo _meal = context.read<MealsRepository>().getMeal(id);
    return Scaffold(
      body: CustomScrollView(slivers: <Widget>[
        SliverBar(
          title: _meal.name,
          header: CacheImage(_meal.getProfilePhoto),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.share),
              onPressed: () => Share.share(
                FlutterI18n.translate(
                  context,
                  'spacex.other.share.capsule.body',
                  translationParams: {
                    'name': _meal.name,
                    'details': Url.shareDetails
                  },
                ),
              ),
              tooltip: FlutterI18n.translate(
                context,
                'spacex.other.menu.share',
              ),
            ),
            PopupMenuButton<String>(
              itemBuilder: (context) => [
                for (final item in Menu.fitnessApp)
                  PopupMenuItem(
                    value: item,
                    child: Text(FlutterI18n.translate(context, item)),
                  )
              ],
              onSelected: (text) => FlutterWebBrowser.openWebPage(
                // url: _meal.url,
                androidToolbarColor: Theme.of(context).primaryColor,
              ),
            ),
          ],
        ),
        SliverSafeArea(
          top: false,
          sliver: SliverToBoxAdapter(
            child: RowLayout.cards(children: <Widget>[
              _descriptionCard(context),
              _specsCard(context),
              _thrustersCard(context),
            ]),
          ),
        ),
      ]),
    );
  }

  Widget _descriptionCard(BuildContext context) {
    final MealInfo _meal = context.read<MealsRepository>().getMeal(id);
    return CardPage.body(
      title: FlutterI18n.translate(
        context,
        'any description',
      ),
      body: RowLayout(children: <Widget>[
        RowIcon(
          FlutterI18n.translate(
            context,
            'text',
          ),
          true,
        ),
        Separator.divider(),
        TextExpand('more text')
      ]),
    );
  }

  Widget _specsCard(BuildContext context) {
    final MealInfo _meal = context.read<MealsRepository>().getMeal(id);
    return CardPage.body(
      title: FlutterI18n.translate(
        context,
        'some text',
      ),
      body: RowLayout(children: <Widget>[
        RowIcon(
          FlutterI18n.translate(
            context,
            'some text',
          ),
          true,
        ),
        Separator.divider(),
        TextExpand('more text')
      ]),
    );
  }

  Widget _thrustersCard(BuildContext context) {
    final MealInfo _meal = context.read<MealsRepository>().getMeal(id);
    return CardPage.body(
      title: FlutterI18n.translate(
        context,
        'some more text',
      ),
      body: RowLayout(children: <Widget>[
        RowIcon(
          FlutterI18n.translate(
            context,
            'some more text',
          ),
          true,
        ),
        Separator.divider(),
        TextExpand('more text')
      ]),
    );
  }
}
