import 'package:dio/dio.dart';
import 'dart:convert';

import '../models/index.dart';
import '../models/info_meal.dart';
import '../services/api_service.dart';
import 'index.dart';

class MealsRepository extends BaseRepository {
  List<MealInfo> meals;
  List<String> photos;

  MealsRepository();

  @override
  Future<void> loadData() async {
    // Try to load the data using [ApiService]
    try {
      // Receives the data and parse it
      final Response mealsResponse = await ApiService().getMeals();
      final Map<String, dynamic> map = mealsResponse.data;
      final List<dynamic> data = map['rows'];

      meals = [
        for (final item in data) MealInfo.fromJson(item),
      ];

      if (photos == null) {
        final indices = List<int>.generate(7, (index) => index)
          ..shuffle()
          ..sublist(0, 5);

        photos = [for (final index in indices) meals[index].getRandomPhoto];
        photos.shuffle();
      }
      finishLoading();
    } catch (_) {
      receivedError();
    }
  }

  MealInfo getMeal(String id) => meals.where((meal) => meal.id == id).first;
}
