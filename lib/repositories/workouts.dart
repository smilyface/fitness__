import 'package:dio/dio.dart';

import '../models/index.dart';
import '../models/info_workout.dart';
import '../services/api_service.dart';
import 'index.dart';

class WorkoutsRepository extends BaseRepository {
  List<WorkoutInfo> workouts;
  List<String> photos;

  WorkoutsRepository();

  @override
  Future<void> loadData() async {
    // Try to load the data using [ApiService]
    try {
      // Receives the data and parse it
      final Response workoutsResponse = await ApiService().getWorkouts();
      final Map<String, dynamic> map = workoutsResponse.data;
      final List<dynamic> data = map['rows'];

      workouts = [
        for (final item in data) WorkoutInfo.fromJson(item),
      ];

      if (photos == null) {
        final indices = List<int>.generate(2, (index) => index)
          ..shuffle()
          ..sublist(0, 1);

        photos = [for (final index in indices) workouts[index].getRandomPhoto];
        photos.shuffle();
      }
      finishLoading();
    } catch (_) {
      receivedError();
    }
  }

  WorkoutInfo getWorkout(String id) =>
      workouts.where((workout) => workout.id == id).first;
}
